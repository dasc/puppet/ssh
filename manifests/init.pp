class ssh (
    Variant[Integer, Array[Integer]] $port,
    Optional[String] $ssh_config_src = undef,
    String $sshd_config,
    String $ssh_group,
    String $ssh_known_hosts,
    Optional[String] $ssh_known_hosts_source = undef,
    String $ssh_known_hosts_perm,
    String $ssh_config_perm,
    String $sftp_subsystem,
    Optional[String] $listen_address = undef,
    Boolean $x11_use_localhost,
    Array[String] $admin_server_ip,
    Array[String] $admin_server_hostname,
    Boolean $service_enable,
    String $service_ensure,
    String $service_name,
    String $package_name,
    String $package_ensure,
    String $local_file_store = 'puppet://puppet/llocds',
    Optional[String] $host_key_group = undef,
    Optional[String] $host_key_perms = undef,
    Optional[String] $host_key_dir = undef,
    Optional[String] $host_cert_group = undef,
    Optional[String] $host_cert_perms = undef,
    Optional[String] $host_cert_dir = undef,
    Enum['yes', 'no', 'without-password'] $permit_root_login,
    Enum['any', 'inet', 'inet6'] $address_family,
    Boolean $challengeresponseauthentication,
    Boolean $gssapiauthentication,
    Boolean $hostbasedauthentication,
    Boolean $kerberosauthentication,
    Boolean $passwordauthentication,
    Boolean $pubkeyauthentication,
    Boolean $rhostsrsaauthentication,
    Boolean $rsaauthentication,
    Boolean $print_motd,
    Optional[Hash[String, String]] $extra_config = undef,
    Optional[Array[String]] $ciphers = undef,
    Array[String] $admin_users,
    Array[Array[String, 2, 2]] $shost_extra_users,
    Array[String] $allow_users,
    Array[String] $deny_users,
    Array[String] $host_keytypes,
    Array[String] $host_certtypes,
    Array[String] $invalid_options,
) {
    package { 'ssh_package' :
        ensure => $package_ensure,
        name   => $package_name,
    }

    service { 'ssh_service':
        ensure  => $service_ensure,
        name    => $service_name,
        enable  => $service_enable,
        require => Package['ssh_package'],
    }
#    file { '/root/.ssh': 
#        ensure => directory, 
#        owner  => 'root', 
#        group  => 'root', 
#        mode   => '0700', 
#    } 
    if $ssh_known_hosts_source {
        file { "${ssh_known_hosts}":
            ensure    => file,
            owner     => 'root',
            group     => 'root',
            mode      => $ssh_known_hosts_perm,
            show_diff => false,
            source    => $ssh_known_hosts_source,
        }
    } else {
        file { "${ssh_known_hosts}":
            ensure    => file,
            owner     => 'root',
            group     => 'root',
            mode      => $ssh_known_hosts_perm,
            show_diff => false,
            source    => ["${local_file_store}/etc/ssh/ssh_known_hosts", ],
        }
    }

    if (length($admin_users) == 0) {
        $rootshostsensure = 'absent'
    } elsif ($::facts['ipaddress'] in $admin_server_ip) {
        $rootshostsensure = 'absent'
    } else {
        $rootshostsensure = 'file'
    }
    file { '/root/.shosts':
        ensure  => $rootshostsensure,
        owner   => 'root',
        group   => 'root',
        mode    => '0600',
        content => epp('ssh/root/.shosts.epp', {
            'admin_server_ip'       => $admin_server_ip,
            'admin_server_hostname' => $admin_server_hostname,
            'admin_users'           => $admin_users,
            'extra_users'           => $shost_extra_users,
        }),
    }
    file { suffix(prefix($host_keytypes, "${host_key_dir}/ssh_host_"), '_key'):
        ensure  => 'file',
        owner   => 'root',
        group   => $host_key_group,
        mode    => $host_key_perms,
        replace => false,
    }

    file { suffix(prefix($host_certtypes, "${host_cert_dir}/ssh_host_"), '_key-cert.pub'):
        ensure  => 'file',
        owner   => 'root',
        group   => $host_cert_group,
        mode    => $host_cert_perms,
        replace => false,
    }
    if $ssh_config_src {
        file { '/etc/ssh/ssh_config':
            ensure => present,
            owner  => 'root',
            group  => $ssh_group,
            mode   => '0644',
            source => $ssh_config_src,
        }
    }

    concat { "${sshd_config}":
        ensure => present,
        owner  => 'root',
        group  => $ssh_group,
        mode   => $ssh_config_perm,
        notify => Service['ssh_service'],
    }


    concat::fragment {'global config':
        target  => $sshd_config,
        order   => 0,
        content => epp('ssh/etc/ssh/sshd_config.epp', {
            'port'                            => $port,
            'sftp_subsystem'                  => $sftp_subsystem,
            'listen_address'                  => $listen_address,
            'host_keytypes'                   => $host_keytypes,
            'host_key_dir'                    => $host_key_dir,
            'host_certtypes'                  => $host_certtypes,
            'host_cert_dir'                   => $host_cert_dir,
            'allow_users'                     => $allow_users,
            'deny_users'                      => $deny_users,
            'invalid_options'                 => $invalid_options,
            'permit_root_login'               => $permit_root_login,
            'challengeresponseauthentication' => $challengeresponseauthentication,
            'gssapiauthentication'            => $gssapiauthentication,
            'hostbasedauthentication'         => $hostbasedauthentication,
            'kerberosauthentication'          => $kerberosauthentication,
            'passwordauthentication'          => $passwordauthentication,
            'pubkeyauthentication'            => $pubkeyauthentication,
            'rhostsrsaauthentication'         => $rhostsrsaauthentication,
            'rsaauthentication'               => $rsaauthentication,
            'address_family'                  => $address_family,
            'x11_use_localhost'               => $x11_use_localhost,
            'ciphers'                         => $ciphers,
            'print_motd'                      => $print_motd,
            'extra_config'                    => $extra_config,
        }),
    }

    lookup('ssh::matchblocks', Hash, 'hash', {}).each |$name, $matchinfo| {
        ssh::matchblock {"${name}":
            * => $matchinfo,
        }
    }
}
