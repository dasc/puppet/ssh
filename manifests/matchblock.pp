define ssh::matchblock (
    Array[Hash] $options,
    String $type,
    Variant[String,Array[String]] $pattern,
    Integer $order = 50,
) {
    include ::ssh

    if $pattern =~ String {
        $stringified_pattern = $pattern
    } else {
        $stringified_pattern = join($pattern, ',')
    }

    concat::fragment { "matchblock ${name}":
	#target  => lookup(ssh::sshd_config),
	target  => $::ssh::sshd_config,
	content => epp("${module_name}/etc/ssh/matchblock_fragment.epp", {
	    'name' => $name,
	    'type' => $type,
	    'pattern' => $stringified_pattern,
	    'options' => $options,
	    'invalid_options' => $::ssh::invalid_options,
	}),
	order   => 200+$order,
    }
}
